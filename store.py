import csv
import os.path

class Store:
    """Simple interface to work with csv files."""
    def __init__(self, filename, fieldnames):
        """Initializes Store object.

        If `filename` doesn't exist, creates it and writes header.

        :param filename: name of csv file
        :param fieldnames: list of fields to be stored
        """
        self.filename = filename
        self.fieldnames = fieldnames
        try:
            self.create_file_if_not_exists()
        except FileNotFoundError:
            print("Couldn't create csv file. Check if path is correct.")

    def create_file_if_not_exists(self):
        """Also writes header with `self.fieldnames`"""
        if not os.path.isfile(self.filename):
            with open(self.filename, 'w', newline='') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=self.fieldnames)
                writer.writeheader()

    def dump_csv(self, data):
        """Overwrites csv file with new data."""
        with open(self.filename, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=self.fieldnames)
            writer.writeheader()
            writer.writerows(data)

    def read_csv(self):
        """Read csv into list."""
        with open(self.filename, 'r', newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            return [row for row in reader]

    def update_csv(self, data, deserialize_func):
        """Update csv file with new data.

        To compare csv and new tweets, they need to be in comparable format.
        That's why we deserialize csv data.

        :param deserialize_func: function to deserialize values read from csv.
        """
        updated = 0
        rows = list(map(deserialize_func, self.read_csv()))
        for i, row in enumerate(rows):
            data_item = [item for item in data if item.get('id', None) == row['id']]
            if data_item and data_item[0] != row:
                rows[i] = data_item[0]
                updated += 1
        unique = [row for row in data if row not in rows]
        rows.extend(unique[::-1])
        self.dump_csv(rows)
        print("Updated: {}, new: {}".format(updated, len(unique)))
