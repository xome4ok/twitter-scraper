FROM alpine:3.4
ADD . /var/scraper
WORKDIR /var/scraper
RUN apk add --no-cache python3 && pip3 install -r requirements.txt &&\
    echo '0 0 * * * cd /var/scraper && python3 main.py' | crontab -
CMD ["/usr/sbin/crond", "-f", "-d", "0"]
