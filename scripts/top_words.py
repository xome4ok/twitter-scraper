import json
from nltk.tokenize import TweetTokenizer
from scripts import stopwords
from main import CONFIG_FILE
from store import Store
from scraper import FIELDS

class Normalizer:
    tokenizer = TweetTokenizer(preserve_case=False, strip_handles=True)

    def normalize(self, string):
        tokens = self.tokenizer.tokenize(string)
        words = [
            token
            for token in tokens
            if token.isalnum() and token not in stopwords.WORDS and token not in stopwords.PUNCTUATION
        ]
        return words

    def top(self, words, number=None):
        vocab = set(words)
        counts = sorted(
            [
                (word, words.count(word))
                for word in vocab
            ],
            key=lambda item: item[1],
            reverse=True
        )
        return counts[:number]


def make_report(rows, top_number):
    normalizer = Normalizer()

    screen_names = set(row['screen_name'] for row in rows)

    words = {
        screen_name: normalizer.normalize(' '.join([
                ' '.join((row['text'], row['quoted_text']))
                for row in rows
                if row['screen_name'] == screen_name
            ]))
        for screen_name in screen_names
        }

    tops = {
        screen_name: normalizer.top(words[screen_name], top_number)
        for screen_name in screen_names
        }

    summary_top = normalizer.top([word for word_list in words.values() for word in word_list], top_number)

    return dict(top_words=tops, top_words_summary=summary_top)


def main():
    data_file = None
    try:
        with open(CONFIG_FILE) as config:
            config_dict = json.load(config)
            data_file = config_dict['output']
            assert data_file, "You have to specify data file to analyze!"
    except:
        print('Invalid configuration. Please check config.json')
        return 1
    try:
        store = Store(data_file, [field[0] for field in FIELDS])
        rows = store.read_csv()
        report = make_report(rows, top_number=10)
        with open('data/top_words.json', 'w', encoding='utf8') as tops_file:
            json.dump(report, tops_file, ensure_ascii=False)
        return

    except Exception as e:
        print('Error has occured in processing. {}'.format(e))
        return 1


if __name__ == '__main__':
    main()
