import json
import scraper


CONFIG_FILE='config/config.json'


def run():
    """Runs scraping process.

    :param config_file: configuration json
    :return: Nothing
    """
    credentials, screen_names, data_file = None, None, None
    try:
        with open(CONFIG_FILE) as config:
            config_dict = json.load(config)
            credentials = scraper.TwitterCredentials(
                config_dict['twitter_credentials']['consumer_key'],
                config_dict['twitter_credentials']['consumer_secret'],
                config_dict['twitter_credentials']['access_token'],
                config_dict['twitter_credentials']['access_token_secret']
            )
            screen_names = config_dict['screen_names']
            data_file = config_dict['output']
            assert screen_names, "You have to provide at least one screen name!"
            assert data_file, "You have to specify output file!"
    except:
        print('Invalid configuration. Please check config.json')
        return 1

    print('Starting twitter client...')
    scraper_session = scraper.TwitterScraper(
        screen_names,
        data_file,
        [field[0] for field in scraper.FIELDS],
        credentials
    )

    print('Twitter client started, collecting tweets...')
    scraper_session.update_users()


if __name__ == '__main__':
    run()
