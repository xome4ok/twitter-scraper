# twitter-scraper

Docker container for twitter scraping.

Given a list of screen names, it periodically (at midnight) scrapes all available (about 3000 tweets tops) tweets from these accounts into csv file.

If some fields are changed in twitterr, they are updated in csv. New tweets are appended.

## Configuration

You can find `config.json` in `config` folder in the root directory of this repo.

It contains one json objects with the following fields:

  * `output` - relative path to file to store collected tweets. I recommend to store them in `data` folder.
  * `twitter_credentials` - api keys from twitter api. You have to obtain this keys at `apps.twitter.com`.
  * `screen_names` - valid json array of strings with twitter screen names.

## Deploy and usage

To deploy this docker container you should have `docker` and `docker-compose` installed on your host system.

All python requirements will be installed automatically, no need to worry about them.

  1. Install `docker-compose` as described at https://docs.docker.com/compose/install/
  2. Clone this repository `git clone https://github.com/xome4ok/twitter-scraper.git`
  3. Go to repository's directory and run `docker-compose up --build`
  4. Congratulations, your container is up and running!
  
## Test if it really works

You can run scraper on demand, without waiting till midnight.

To do that:
  1. Go to repository's directory
  2. Execute `docker-compose exec twitter-scraper python3 /var/scraper/main.py`

## Extracted fields

Extraction of specific fields requires coding, so if you want additional fields to be extracted, you'll need to change `FIELDS` list in `scraper.py`.

* `screen_name` - as in `@twitter`, but without `@`
* `id` - unique id of tweet
* `created_at` - date and time in UTC+0
* `user_mentions` - all users mentioned in this tweet using `@user` syntax
* `hashtags` - all hashtags from this tweet without `#`
* `retweet_count` - in case of retweet, shows retweets of original tweet
* `favorite_count` - doesn't indicate the fact of favoriting of retweet, only for owned tweets
* `text` - the text written by account owner. Doesn't include text of quoted tweet. In case of retweet contains retweeted tweet's text.
* `retweeted` - is this tweet a retweet?
* `quoted_status` - do this tweet include a quote?
* `quoted_created_at` 
* `quoted_id`
* `quoted_screen_name`
* `quoted_name`
* `quoted_hashtags`
* `quoted_retweet_count`
* `quoted_favorite_count`
* `quoted_text`

## Top words script

In `scripts` directory there's a `top_words.py` script. When you execute it, it takes data file as defined in `config/config.json` 
and counts top ten words:

* for every twitter account in dataset
* for whole dataset

Result will be stored in `data/top_words.json` file with following structure:

```json
{
  "top_words": {
    "user1": [["word1", count_for_word1], ["word2", count_for_word2], ...]
    "user2": ...,
    ...
  },
    
  "top_words_summary": [["word3", count_for_word3], ...]
}
```