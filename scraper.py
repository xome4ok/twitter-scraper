import ast
import datetime
import tweepy.utils
import tweets
import store


def parse(value):
    """Try to parse value.

    If value is empty, returns empty string, if value cannot be parsed returns string representation and outputs an error.
    Otherwise, returns parsed value.
    """
    try:
        if not value:
            return ''
        return ast.literal_eval(value)
    except:
        print("Error parsing {}".format(value))
        return str(value)


def parse_datetime(dt):
    """Try to parse datetime value from iso format.

    If value is empty, returns empty string, if value cannot be parsed returns string representation and outputs an error.
    Otherwise, returns isoformatted datetime.
    """
    try:
        if not dt:
            return ''
        return datetime.datetime.strptime(dt, '%Y-%m-%dT%H:%M:%S').isoformat()
    except:
        print("Error parsing datetime: {}".format(dt))
        return str(dt)


def parse_tweepy_datetime(dt):
    """Try to parse datetime using tweepy.

    Must be used to parse datetimes from tweets, esp. from quoted ones.
    """
    if not dt:
        return ''
    return tweepy.utils.parse_datetime(dt).isoformat()


FIELDS = [
    ('screen_name', lambda status: status.user.screen_name, str),
    ('id', lambda status: status.id_str, str),
    ('created_at', lambda status: status.created_at.isoformat(), parse_datetime),
    ('user_mentions', lambda status: [user['screen_name'] for user in status.entities['user_mentions']], parse),
    ('hashtags', lambda status: [hashtag['text'] for hashtag in status.entities['hashtags']], parse),
    ('retweet_count', lambda status: status.retweet_count, parse),
    ('favorite_count', lambda status: status.favorite_count, parse),
    ('text', lambda status: getattr(getattr(status,'retweeted_status', None),'text', None) or status.text, str),
    ('retweeted', lambda status: hasattr(status, 'retweeted_status'), parse),  # is retweet?
    ('quoted_status', lambda status: hasattr(status, 'quoted_status'), parse),  # is quoted?
    ('quoted_created_at', lambda status: parse_tweepy_datetime(getattr(status, 'quoted_status', dict()).get('created_at', None)), parse_datetime),
    ('quoted_id', lambda status: getattr(status, 'quoted_status', dict()).get('id_str', ''), str),
    ('quoted_screen_name', lambda status: getattr(status, 'quoted_status', dict()).get('user', dict()).get('screen_name', ''), str),
    ('quoted_name', lambda status: getattr(status, 'quoted_status', dict()).get('user', dict()).get('name', ''), str),
    ('quoted_hashtags', lambda status: [hashtag['text'] for hashtag in getattr(status, 'quoted_status', dict()).get('entities', dict()).get('hashtags', [])], parse),
    ('quoted_retweet_count', lambda status: getattr(status, 'quoted_status', dict()).get('retweet_count', ''), parse),
    ('quoted_favorite_count', lambda status: getattr(status, 'quoted_status', dict()).get('favorite_count', ''), parse),
    ('quoted_text', lambda status: getattr(status, 'quoted_status', dict()).get('text', ''), str)
]
"""FIELDS: field_name in csv, lambda for extracting from tweet, parsing function"""


class TwitterCredentials:
    """Class to store twitter keys."""
    def __init__(self, consumer_key, consumer_secret, access_token, access_token_secret):
        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret
        self.access_token = access_token
        self.access_token_secret = access_token_secret


class TwitterScraper:
    """Main scraping interface."""
    def __init__(self, screen_names, filename, fieldnames, twitter_credentials):
        """Initializes TwitterScraper instance.

        :param screen_names: list of screen names to be tracked
        :param filename: file where data will be stored
        :param fieldnames: fields to extract from tweet (scraper.FIELDS)
        :param twitter_credentials: TwitterCredentials object
        """
        self.screen_names = screen_names
        self.filename = filename
        self.fieldnames = fieldnames
        self.store = store.Store(filename, fieldnames)
        self.twitter_credentials = twitter_credentials

    def tweet_as_dict(self, tweet):
        """Converts Tweepy Status object to dict."""
        dict_repr = {
            key: getter_func(tweet)
            for key, getter_func, _
            in FIELDS
        }
        for key, item in dict_repr.items():
            if isinstance(item, str):
                dict_repr[key] = item.replace(';','')
        return dict_repr

    def deserialize_tweet(self, tweet_dict):
        """Deserializes dict's values."""
        return {
            key: ([func for name, _, func in FIELDS if name == key] or [parse])[0](value)
            for key, value
            in tweet_dict.items()
        }

    def update_users(self):
        """Actualizes csv file according to recent twitter data."""
        twitter = tweets.TwitterHarvester(
            self.twitter_credentials.consumer_key,
            self.twitter_credentials.consumer_secret,
            self.twitter_credentials.access_token,
            self.twitter_credentials.access_token_secret,
            wait_on_rate_limit=True,
            wait_on_rate_limit_notify=True
        )
        tweets_collection = twitter.get_by_users(self.screen_names)
        prepared_for_dump = []
        for screen_name, user_tweets in tweets_collection.items():
            print('Updating {}'.format(screen_name))
            for tweet in user_tweets:
                prepared_for_dump.append(self.tweet_as_dict(tweet))
        self.store.update_csv(prepared_for_dump, self.deserialize_tweet)
