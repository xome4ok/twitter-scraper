from typing import List

import time
import tweepy


class TwitterHarvester(object):
    """Creates a new TwitterHarvester instance"""
    def __init__(self, consumer_key, consumer_secret,
                 access_token, access_token_secret,
                 wait_on_rate_limit=False,
                 wait_on_rate_limit_notify=False):

        self.auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        self.auth.secure = True
        self.auth.set_access_token(access_token, access_token_secret)
        self.api = tweepy.API(
            self.auth,
            wait_on_rate_limit=wait_on_rate_limit,
            wait_on_rate_limit_notify=wait_on_rate_limit_notify
        )

    def get_by_users(self, screen_names: List[str]):
        """Gets most recent tweets from list of usernames.

        Due to Twitter API limitations it's possible to get
        only about 3000 latest tweets per user.

        :param screen_names: list of screen_names
        :return: dict {screen_name: list of collected tweets} 
        """
        collected = dict()
        for screen_name in screen_names:
            print('Fetching tweets for {}'.format(screen_name))
            current_user_tweets = self.get_by_user(screen_name)
            collected[screen_name] = current_user_tweets
            print('{} tweets collected'.format(len(current_user_tweets)))
        return collected

    def get_by_user(self, screen_name: str):
        """Gets most recent tweets from username.

        Due to Twitter API limitations it's possible to get
        only about 3000 latest tweets.

        :param screen_name: screen_name without @
        :param api: tweepy.API
        :return: list of recent tweets from user
        """
        tweets_provider = tweepy.Cursor(self.api.user_timeline, screen_name=screen_name).items()
        tweets = []
        retry_counter = 0
        while True:
            try:
                tweet = tweets_provider.next()
                tweets.append(tweet)
            except StopIteration:
                break
            except tweepy.TweepError as e:
                print(
                    "Something went wrong. \nCheck your twitter credentials. Check screen_name '{}' is not valid twitter name.\n{}".format(
                        screen_name,
                        e
                    )
                )
                retry_counter += 1
                if retry_counter >= 10:
                    print("Giving up.")
                    break
                else:
                    print("Retry {} out of 10...".format(retry_counter))
                    time.sleep(10)
                    continue
        return tweets
